# MPVM

High level virtual machine originally designed for the MPlus language

(Currently only capable of disassembling code)

## Build Instruction

Requires CMake 3.1 (or above) and C99 (or higher) supporting compiler.

```
# Linux

mkdir build
cd build
cmake ..
make
```

## Use Instruction

`mpvm bytecode_file`

## Bytecode Format

| Offset                                | Size               | Meaning                                               |
|---------------------------------------|--------------------|-------------------------------------------------------|
| 0 to 4                                | 4 bytes            | Magic Number: Must be `0xBC 0x14 0x70 0x01`           |
| 5 to 9                                | 4 bytes Big endian | Constant pool size (`cpsize`)                         |
| 10 to 14                              | 4 bytes Big endian | Code size (`cbsize`)                                  |
| 15 to `cpend`                         | at least `cpsize`  | Each constant is encoded by a 4-byte length and data  |
| `cpend` + 1 to `cpend` + 1 + `cbsize` | `cbsize`           | The actual opcodes. Jumps are relative to `cpend` + 1 |

## Opcodes

Missing values are detonated by `??` grouped and encoded in big endian order.

| Byte Format (Hex) | Name      | Meaning                               |
|-------------------|-----------|---------------------------------------|
| 00 (??)           | ldb       | Load variable with name (from constant pool) `??`      |
| 01 (?? ?? ?? ??)  | ldi       | Load variable with name (from constant pool) `??`      |
| 02 (??)           | ldc.i8    | Load number `??`                      |
| 03 (?? ??)        | ldc.i16   | Load number `??`                      |
| 04 (?? ?? ?? ??)  | ldc.i32   | Load number `??`                      |
| 05 (?? ?? ?? ?? ?? ?? ?? ??) | ldc.i64 | Load number `??`            |
| 06 (?? ?? ?? ??)  | ldc.f32   | Load IEEE float `??`                  |
| 07 (?? ?? ?? ?? ?? ?? ?? ??) | ldc.f64 | Load IEEE double `??`       |
| 08 (?? ?? ?? ??)  | ldc.str   | Load from constant pool `??`          |
| 09                | ldc.nil   | Load nil                              |
| 0A (?? ?? ?? ??)  | ldc.array | Pop `??` values and load as an array  |
| 0B (??)           | stb       | Store variable with name (from constant pool) `??`            |
| 0C (?? ?? ?? ??)  | sti       | Store variable with name (from constant pool) `??`            |
| 0D (??)           | declb     | Declare variable with name (from constant pool) `??`          |
| 0E (?? ?? ?? ??)  | decli     | Declare variable with name (from constant pool) `??`          |
| 0F                | ucall     | Pop value and apply it with no parameters                     |
| 10 (?? ?? ?? ??)  | call      | Pop `??` parameters, value and apply it with `??` parameters  |
| 11                | ret       | Return from a function (resets state)                         |
| 12                | yield     | Yield from a function (does not reset state)                  |
| 13 (?? ?? ?? ??)  | jmp       | Jumps to address `??`                                         |
| 14 (?? ?? ?? ??)  | jmp.true  | Pop value and jump to address `??` if the value is truthy     |
| 15 (?? ?? ?? ??)  | jmp.false | Pop value and jump to address `??` if the value is falsey     |
| 16                | dup       | Duplicate top of stack                                        |
| 17                | pop       | Remove top of stack                                           |
| 18                | swp       | Swaps the top two elements of stack                           |
| 19 (?? ?? ?? ??)  | func.load | Marks the next address as a function object and jumps to `??` |
| 1A (?? ?? ?? ??)  | func.map  | Maps a parameter (name provided by ?? in constant pool) to a function |
| 1B (?? ?? ?? ??)  | func.omap | Maps an optional parameter (name provided by ?? in constant pool) to a function. Popped value is the default value of the parameter |
| 1C                | func.varargs | Marks the function as a varargs function |
| 1D (?? ?? ?? ??)  | lazy.load | Marks the next address for delay execution and jumps to ??    |
| 1E                | err.throw | Pops a value and throws it as an error                        |
| 1F (?? ?? ?? ??)  | err.handle | Jumps to ?? if error happens within this opcode and address ?? |
| 20                | err.rethrow | Rethrows an error, cannot be used with err.decl           |
| 21 (?? ?? ?? ??)  | err.decl  | Declares a variable (name provided by ?? in constant pool) with the intercepted error, cannot be used with err.rethrow |
| 22                | print     | Pops value and prints it to standard output                   |