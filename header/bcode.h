#ifndef MPLUS_BCODE_H__
#define MPLUS_BCODE_H__

#include <stdio.h>
#include <stddef.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#include <endian.h>

enum read_state
  {
    SUCCESS = 0, BAD_MAGIC = 1, BAD_CPSIZE = 2, BAD_CBSIZE = 3,
    BAD_CP_ENT = 4, BAD_CB_ENT = 5
  };

typedef struct bcode_t
{
  size_t cplen;
  size_t cblen;
  uint8_t **cp;
  uint8_t *cb;
} bcode_t;

void init_bcode (bcode_t *self, const size_t cplen, const size_t cblen);

enum read_state read_bcode (bcode_t *self, FILE *src);

void delete_bcode (bcode_t *self);

#endif /* !MPLUS_BCODE_H__ */
