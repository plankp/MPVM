#ifndef MPLUS_DISASM_H__
#define MPLUS_DISASM_H__

#include "bcode.h"
#include "utils.h"

#include <stdio.h>
#include <stddef.h>
#include <stdint.h>
#include <inttypes.h>

void disassemble (const bcode_t *code, FILE *output);

#endif /* !MPLUS_DISASM_H__ */
