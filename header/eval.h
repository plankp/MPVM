#ifndef MPLUS_EVAL_H__
#define MPLUS_EVAL_H__

#include "utils.h"

#include <stddef.h>
#include <stdint.h>

void eval (const char **kmap, const uint8_t *bytes, const size_t len);

#endif /* !MPLUS_EVAL_H__ */
