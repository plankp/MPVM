#ifndef MPLUS_UTILS_H__
#define MPLUS_UTILS_H__

#include <stdint.h>

#include <endian.h>

union f32c
{
  float f;
  uint32_t i;
};

union f64c
{
  double f;
  uint64_t i;
};

#define GET_I8(ptr)  (*ptr)
#define GET_I16(ptr) (be16toh(*(uint16_t*)ptr))
#define GET_I32(ptr) (be32toh(*(uint32_t*)ptr))
#define GET_I64(ptr) (be64toh(*(uint64_t*)ptr))
#define GET_F32(ptr) (((union f32c){.i=GET_I32(ptr)}).f)
#define GET_F64(ptr) (((union f64c){.i=GET_I64(ptr)}).f)

#endif /* !MPLUS_UTILS_H__ */
