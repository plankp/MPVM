#include "bcode.h"
#include "disasm.h"

int
main (int argc, char **argv)
{
  if (argc != 2)
    {
      fprintf(stderr, "Expected file to disassemble");
      return 1;
    }

  FILE *fptr = fopen(argv[1], "r");
  if (!fptr)
    {
      fprintf(stderr, "Cannot open file %s", argv[1]);
      return 2;
    }

  int ret;
  bcode_t code;
  if ((ret = read_bcode(&code, fptr)))
    {
      /* Add info on what failed */
      fprintf(stderr, "Cannot read file %s", argv[1]);
      fclose(fptr);
      return 2;
    }

  disassemble(&code, stdout);

  size_t i;
  for (i = 0; i < code.cplen; ++i) free(code.cp[i]);
  delete_bcode(&code);
  fclose(fptr);
  return 0;
}
