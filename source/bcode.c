#include "bcode.h"

void
init_bcode (bcode_t *self, const size_t cplen, const size_t cblen)
{
  self->cplen = cplen;
  self->cblen = cblen;
  self->cp = malloc(cplen * sizeof (uint8_t *));
  self->cb = malloc(cblen * sizeof (uint8_t));
}

#define MAGIC_SIZE 4

const static uint8_t VALID_MAGIC_BYTES[MAGIC_SIZE] =
  { 0xBC, 0x14, 0x70, 0x01 };

enum read_state
read_bcode (bcode_t *self, FILE *src)
{
  uint8_t magic[MAGIC_SIZE];
  uint32_t cpsize;
  uint32_t cbsize;
  
  if (fread(magic, sizeof (uint8_t), MAGIC_SIZE, src) != MAGIC_SIZE)
    {
      return BAD_MAGIC;
    }
  if (memcmp(VALID_MAGIC_BYTES, magic, MAGIC_SIZE) != 0)
    {
      return BAD_MAGIC;
    }

  if (fread(&cpsize, sizeof (uint32_t), 1, src) != 1)
    {
      return BAD_CPSIZE;
    }
  if (fread(&cbsize, sizeof (uint32_t), 1, src) != 1)
    {
      return BAD_CBSIZE;
    }

  cpsize = be32toh(cpsize);
  cbsize = be32toh(cbsize);

  bcode_t local;
  init_bcode(&local, cpsize, cbsize);

  size_t i;
  for (i = 0; i < cpsize; ++i)
    {
      uint32_t klen;
      if (fread(&klen, sizeof (uint32_t), 1, src) != 1)
	{
	  delete_bcode(&local);
	  return BAD_CP_ENT;
	}

      klen = be32toh(klen);
      
      uint8_t *buf = malloc(klen * sizeof (uint8_t));
      if (fread(buf, sizeof (uint8_t), klen, src) != klen)
	{
	  free(buf);
	  delete_bcode(&local);
	  return BAD_CP_ENT;
	}

      local.cp[i] = buf;
    }

  if (fread(local.cb, sizeof (uint8_t), cbsize, src) != cbsize)
    {
      for (i = 0; i < cpsize; ++i)
	{
	  free(local.cp[i]);
	}
      delete_bcode(&local);
      return BAD_CB_ENT;
    }

  *self = local;
  return SUCCESS;
}

#undef MAGIC_SIZE

void
delete_bcode (bcode_t *self)
{
  if (!self) return;

  free(self->cp);
  free(self->cb);
}
