#include "eval.h"

void
eval (const char **kmap, const uint8_t *bytes, const size_t len)
{
#if 0
  size_t i;
  for (i = 0; i < len; ++i)
    {
      fprintf(output, "0x%04zx\t", i);
      switch (bytes[i])
	{
	case 0:
	  fprintf(output, "ldb %s", kmap[bytes[++i]]);
	  break;
	case 1:
	  fprintf(output, "ldi %s", kmap[GET_I32(&bytes[++i])]);
	  i += 3;
	  break;
	case 2:
	  fprintf(output, "ldc.i8 %" PRId8, bytes[++i]);
	  break;
	case 3:
	  fprintf(output, "ldc.i16 %" PRId16, GET_I16(&bytes[++i]));
	  ++i;
	  break;
	case 4:
	  fprintf(output, "ldc.i32 %" PRId32, GET_I32(&bytes[++i]));
	  i += 3;
	  break;
	case 5:
	  fprintf(output, "ldc.i64 %" PRId64, GET_I64(&bytes[++i]));
	  i += 7;
	  break;
	case 6:
	  fprintf(output, "ldc.f32 %g", GET_F32(&bytes[++i]));
	  i += 3;
	  break;
	case 7:
	  fprintf(output, "ldc.f64 %g", GET_F64(&bytes[++i]));
	  i += 7;
	  break;
	case 8:
	  fprintf(output, "ldc.str %s", kmap[GET_I32(&bytes[++i])]);
	  i += 3;
	  break;
	case 9:
	  fprintf(output, "ldc.nil");
	  break;
	case 10:
	  fprintf(output, "ldc.array");
	  break;
	case 11:
	  fprintf(output, "stb %s", kmap[bytes[++i]]);
	  break;
	case 12:
	  fprintf(output, "sti %s", kmap[GET_I32(&bytes[++i])]);
	  i += 3;
	  break;
	case 13:
	  fprintf(output, "declb %s", kmap[bytes[++i]]);
	  break;
	case 14:
	  fprintf(output, "decli %s", kmap[GET_I32(&bytes[++i])]);
	  i += 3;
	  break;
	case 15:
	  fprintf(output, "ucall");
	  break;
	case 16:
	  fprintf(output, "call %" PRId32, GET_I32(&bytes[++i]));
	  i += 3;
	  break;
	case 17:
	  fprintf(output, "ret");
	  break;
	case 18:
	  fprintf(output, "yield");
	  break;
	case 19:
	  fprintf(output, "jmp 0x%04" PRIx32, GET_I32(&bytes[++i]));
	  i += 3;
	  break;
	case 20:
	  fprintf(output, "jmp.true 0x%04" PRIx32, GET_I32(&bytes[++i]));
	  i += 3;
	  break;
	case 21:
	  fprintf(output, "jmp.false 0x%04" PRIx32, GET_I32(&bytes[++i]));
	  i += 3;
	  break;
	case 22:
	  fprintf(output, "dup");
	  break;
	case 23:
	  fprintf(output, "pop");
	  break;
	case 24:
	  fprintf(output, "swp");
	  break;
	case 25:
	  fprintf(output, "func.load 0x%04" PRIx32, GET_I32(&bytes[++i]));
	  i += 3;
	  break;
	case 26:
	  fprintf(output, "func.map %s", kmap[GET_I32(&bytes[++i])]);
	  i += 3;
	  break;
	case 27:
	  fprintf(output, "func.omap %s", kmap[GET_I32(&bytes[++i])]);
	  i += 3;
	  break;
	case 28:
	  fprintf(output, "func.varargs");
	  break;
	case 29:
	  fprintf(output, "lazy.load 0x%04" PRIx32, GET_I32(&bytes[++i]));
	  i += 3;
	  break;
	case 30:
	  fprintf(output, "err.throw");
	  break;
	case 31:
	  fprintf(output, "err.handle 0x%04" PRIx32, GET_I32(&bytes[++i]));
	  i += 3;
	  break;
	case 32:
	  fprintf(output, "err.rethrow");
	  break;
	case 33:
	  fprintf(output, "err.decl %s", kmap[GET_I32(&bytes[++i])]);
	  i += 3;
	  break;
	case 34:
	  fprintf(output, "print");
	  break;
	case 35:
	  fprintf(output, "ld.add");
	  break;
	case 36:
	  fprintf(output, "ld.sub");
	  break;
	case 37:
	  fprintf(output, "ld.mul");
	  break;
	case 38:
	  fprintf(output, "ld.div");
	  break;
	case 39:
	  fprintf(output, "ld.mod");
	  break;
	case 40:
	  fprintf(output, "ld.pow");
	  break;
	case 41:
	  fprintf(output, "ld.percent");
	  break;
	case 42:
	  fprintf(output, "ld.cons");
	  break;
	case 43:
	  fprintf(output, "ld.gt");
	  break;
	case 44:
	  fprintf(output, "ld.lt");
	  break;
	case 45:
	  fprintf(output, "ld.ge");
	  break;
	case 46:
	  fprintf(output, "ld.le");
	  break;
	case 47:
	  fprintf(output, "ld.eql");
	  break;
	case 48:
	  fprintf(output, "ld.neq");
	  break;
	case 49:
	  fprintf(output, "ld.not");
	  break;
	case 50:
	  fprintf(output, "ld.and");
	  break;
	case 51:
	  fprintf(output, "ld.or");
	  break;
	case 52:
	  fprintf(output, "ld.fcmp");
	  break;
	default:
	  fprintf(output, "[unrecognized %d]", bytes[i]);
	  break;
	}
      fprintf(output, "\n");
    }
#endif
}
